import Vue from "vue";
import VueRouter from "vue-router";
import HomePage from "../pages/HomePage.vue";
import LoginPage from "../pages/LoginPage.vue";
import RegisterPage from "../pages/RegisterPage";
import Task from "../pages/AddTask";
import Workhour from "../pages/AddWorkhour";
import Expen from "../pages/AddExpen";
import Expentask from "../pages/AddExpentask";
import Excel from "../pages/Excel.vue";
import AlluserWorklists from "../pages/AlluserWorklists.vue";
import CooPage from "../pages/CooPage.vue";
import NotFound from "../pages/NotFound.vue";
import Pdfpage from "../components/layouts/PdfPage.vue";

import User from "../components/user/UserList";
import UserDetail from "../components/user/UserDetail";
import WorkhourDetail from "../components/worklist/WorkhourDetail";
import DaysOffLists from "../components/daysoff/DaysOffLists.vue";
import AdminUserlists from "../components/coo/AdminUserlists.vue";
import AdmincheckedMembers from "../components/coo/AdmincheckedMembers.vue";

Vue.use(VueRouter);
const routes = [
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/home",
    name: "HomePage",
    component: HomePage,
  },
  {
    path: "/register",
    name: "RegisterPage",
    component: RegisterPage,
    meta: { guest: true },
  },
  {
    path: "/login",
    name: "LoginPage",
    component: LoginPage,
    meta: { guest: true, title: "Entrance" },
  },

  {
    path: "/userbydp",
    name: "User",
    component: User,
  },
  {
    path: "/task",
    name: "Task",
    component: Task,
  },
  {
    path: "/expentask",
    name: "Expentask",
    component: Expentask,
  },
  {
    path: "/workhour",
    name: "Workhour",
    component: Workhour,
  },
  {
    path: "/expen",
    name: "Expen",
    component: Expen,
  },
  {
    path: "/excel",
    name: "Excel",
    component: Excel,
  },
  {
    path: "/user/:id",
    name: "UserDetail",
    component: UserDetail,
  },
  {
    path: "/workhour/:id",
    name: "WorkhourDetail",
    component: WorkhourDetail,
  },
  {
    path: "/daysoff",
    name: "DaysOffLists",
    component: DaysOffLists,
  },
  {
    path: "/coo",
    name: "CooPage",
    component: CooPage,
  },
  {
    path: "/alluserworklists",
    name: "AlluserWorklists",
    component: AlluserWorklists,
  },
  {
    path: "/adminUserlists",
    name: "AdminUserlists",
    component: AdminUserlists,
  },
  {
    path: "/admincheckedMembers",
    name: "AdmincheckedMembers",
    component: AdmincheckedMembers,
  },
  {
    path: "/pdf",
    name: "Pdfpage",
    component: Pdfpage,
  },
  { path: "/:notFound(.*)", component: NotFound },
];
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
