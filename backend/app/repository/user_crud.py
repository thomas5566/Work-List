from sqlalchemy.orm import Session, contains_eager
from sqlalchemy import and_, func, or_
from ..models import User, Workhour, Department
from ..schemas import users


def get_user(db: Session, user_id: int):
    return db.query(User).filter(User.id == user_id).first()


def get_user_by_username(db: Session, username: str):
    return db.query(User).filter(User.username == username).first()


def get_user_by_department(db: Session, department_id: str, user_id: int):
    return db.query(User).filter(User.department_id == department_id)\
        .filter(User.is_active != 'false').filter(User.id != user_id).all()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(User).filter(User.is_active != 'false').offset(skip).limit(limit).all()


def get_hrcheck_allusers(db: Session, user_id: int):
    query = (db.query(User)
             .join(Workhour)
             .join(Department)
             # @note: here we are tricking sqlalchemy to think that we loaded all these relationships,
             # even though we filter them out by version. Please use this only to get data and display,
             # but not to continue working with it as if it were a regular UnitOfWork
             .options(
             contains_eager(User.workhours).
             contains_eager(Workhour.user)
        # contains_eager(user.User.department)
    )
        .filter(User.id == Workhour.user_id)
        .filter(or_(Workhour.hr_checked == "false", Workhour.manager_checked == "false"))
        .filter(User.is_active != 'false')
        .filter(User.id != user_id)
        # .filter(extract('year', Workhour.date) == 2021)
        # .filter(extract('month', Workhour.date) == 7)
        .order_by(Workhour.date.desc())
    ).all()

    return query


def get_admin_allusers(db: Session):
    query = (db.query(User)
             .join(Workhour)
             .join(Department)
             .options(
        contains_eager(User.workhours).
        contains_eager(Workhour.user)
    )
        .filter(User.id == Workhour.user_id)
        .filter(User.is_active != 'false')
        .filter(Workhour.hr_checked == "true")
        .order_by(Workhour.date.desc())
    ).all()

    return query


def create_user(db: Session, user_item: users.UserCreate):
    db_user = User(
        username=user_item.username,
        # fullname=user.fullname,
        password=user_item.password,
        department_id=user_item.department_id,
        checklistAll_permission=user_item.checklistAll_permission,
        # is_active=True,
        # is_superuser=False
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user
