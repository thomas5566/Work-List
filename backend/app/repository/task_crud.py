from sqlalchemy.orm import Session, contains_eager
from sqlalchemy import func, text
from fastapi import HTTPException, status
# from .. import models, schemas

from ..models import Task, Workhour, User
from ..schemas import tasks


def get_task(db: Session, task_id: int):
    return db.query(Task).filter(Task.id == task_id).first()


def get_task_by_taskname(db: Session, taskname: str):
    return db.query(Task).filter(Task.taskname == taskname).first()


def get_tasks(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Task).filter(Task.is_active == "true").offset(skip).limit(limit).all()


def get_tasks_by_worklist(db: Session):
    q = (db.query(Task)
         .join(Workhour)
         .options(
        contains_eager(Task.workhours).
        contains_eager(Workhour.task)
    )
        .filter(Task.id == Workhour.task_id)
        .order_by(Workhour.date.desc())
    ).all()

    return q


def get_tasktotalhour_by_users(db: Session):
    qry = (db.query(
        Task.taskname,
        User.username,
        func.to_char(Workhour.date, 'YYYY-MM').label('year_month'),
        func.sum(Workhour.hour).label('total_hour'))
        .join(Workhour, Task.id == Workhour.task_id)
        .join(User, User.id == Workhour.user_id)
        .filter(User.is_active == 'true')
        .group_by('year_month', Task.taskname, User.id)
        .order_by(text("year_month desc"))
        .all()
    )
    print(qry)
    return qry


def create_task(db: Session, task_items: tasks.TaskCreate):
    db_task = Task(
        taskname=task_items.taskname,
        fullname=task_items.fullname,
        organization=task_items.organization)
    db.add(db_task)
    db.commit()
    db.refresh(db_task)
    return db_task


def update_task(db: Session, task_id: int, task_items: tasks.TaskUpdate):
    db_task = db.query(Task).filter(Task.id == task_id).first()
    if db_task is None:
        return None
    for var, value in vars(task_items).items():
        setattr(db_task, var, value) if value else None
    db.add(db_task)
    db.commit()
    db.refresh(db_task)
    return db_task


def delete_task(id: int, db: Session):
    task_item = db.query(Task).filter(
        Task.id == id)

    if not task_item.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Expen with id {id} not found")

    task_item.delete(synchronize_session=False)
    db.commit()
    return "Task hase being delete"
