from pydantic import BaseModel
from typing import List


class UserLists(BaseModel):
    id: int
    username: str
    fullname: str
    is_active: bool

    class Config:
        orm_mode = True


class DepartmentBase(BaseModel):
    department_name: str
    is_activate: bool


class Department(DepartmentBase):
    id: int
    user: List[UserLists] = []

    class Config:
        orm_mode = True
