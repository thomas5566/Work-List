from pydantic import BaseModel


class FlowBase(BaseModel):
    user_name: str
    check_sort: int