from typing import Optional

from sqlalchemy.sql.expression import false
from pydantic import BaseModel
import datetime


class HrworklistBase(BaseModel):
    user_id: Optional[int] = None
    start_date: Optional[datetime.date]
    end_date: Optional[datetime.date]
    description: Optional[str] = None
    # admin1_checked: Optional[bool] = False


class HrworklistUpdate(HrworklistBase):
    pass


class HrworklistCreate(HrworklistBase):
    pass


class Hrworklists(HrworklistBase):
    id: int

    class Config:
        orm_mode = True
